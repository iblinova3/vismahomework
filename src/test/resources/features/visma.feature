Feature: User is able to see Visma.com landing page and navigate in the site

  #Should cover points 1-3.1,3.2
  Scenario: Presentation form mandatory fields check
    Given user navigates to page https://www.visma.lv/
    And user clicks presentation button
    And presentation page is opened
    When user clicks submit button
    Then error appear under field
      | field        | error                  |
      | firstName    | Šis lauks ir obligāts. |
      | lastName     | Šis lauks ir obligāts. |
      | company      | Šis lauks ir obligāts. |
      | busPhone     | Šis lauks ir obligāts. |
      | emailAddress | Šis lauks ir obligāts. |
    #Here we check that user is not redirected
    And presentation page is opened
    When user fills in data
      | field        | data          |
      | firstName    | testFN        |
      | lastName     | testLN        |
      | company      | testCompany   |
      | busPhone     | testPhone     |
      | emailAddress | test@test.com |
    And user clicks submit button
    Then error Mums ir nepieciešama Jūsu piekrišana Šis lauks ir obligāts. appears under checkbox
    And presentation page is opened
    When user clicks check box
    And user clicks submit button
    Then form is saved successfully with text Paldies!

  #Should cover points 3.3
  Scenario Outline: Presentation form email check
    Given user navigates to page https://www.visma.lv/
    And user clicks presentation button
    And presentation page is opened
    When user fills in data
      | field        | data    |
      | emailAddress | <email> |
    And user clicks submit button
    Then error appear under field
      | field        | error                          |
      | emailAddress | Ievadiet derîgu epasta adresi. |
    Examples:
      | email       |
      | test        |
      | test@test   |
      | test.test   |
      | test@test.c |
      | @test.com   |

    #Should cover point 4
  Scenario: Blog ling generation check
    Given user navigates to page https://www.visma.lv/
    When click on active blog link
    Then opens blog page in new tab
    And goes back and click on active blog link
    And opens blog page in new tab
    And goes back and click on active blog link
    And opens blog page in new tab
    And goes back and click on active blog link
    And opens blog page in new tab
    Then opened blog pages should be different

    #Should cover point 5
  Scenario Outline: Social Links check
    Given user navigates to page https://www.visma.lv/
    When click on <link> in footer
    Then page with <url> is opened in new tab
    Examples:
      | link     | url                                         |
      | Facebook | https://www.facebook.com/VISMALatvia/       |
      | LinkedIn | https://www.linkedin.com/company/fms-group/ |
      | Blog     | https://www.visma.lv/blogs/                 |

     #Should cover point 6
  Scenario Outline: Location change check
    Given user navigates to page https://www.visma.lv/
    When I select <country> from dropdown
    Then page with <url> is opened
    And page language is <language>
    Examples:
      | country     | url                      | language |
      | Denmark     | https://www.visma.dk/    | da       |
      | Finland     | https://www.visma.fi/    | fi       |
      | Lithuania   | https://www.visma.lt/    | lt       |
      | Netherlands | https://nl.visma.com/    | nl       |
      | Norway      | https://www.visma.no/    | nb       |
      | Romania     | https://www.visma.ro/    | ro       |
      | Sweden      | https://www.visma.se/    | sv       |
      | UK          | https://www.visma.co.uk/ | en       |
      | Visma.com   | https://www.visma.com/   | en       |