package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Driver;


public class HomePage {

    WebDriver driver;
    WebDriverWait wait = new WebDriverWait(Driver.getInstance(), 10);
    @FindBy(css = "div.bannerContentBody a.cta")
    private WebElement presentationButton;
    @FindBy(xpath = "//ul//li[@style!='display: none;']//a")
    private WebElement activeBlogLink;
    @FindBy(id = "countrychooser")
    private WebElement countryDropdown;
    @FindBy(css = "ul.select2-results__options")
    private WebElement countryList;
    @FindBy(xpath = "//html")
    private WebElement pageLanguage;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickPresentationButton() {
        wait.until(ExpectedConditions.visibilityOf(presentationButton));
        presentationButton.click();
    }

    public void getActiveBlogLink() {
        wait.until(ExpectedConditions.visibilityOf(activeBlogLink));
        activeBlogLink.click();
    }

    public void clickSocialLink(String name) {
        driver.findElement(By.linkText(name)).click();
    }

    private WebElement countryLink(String country) {
        return countryList.findElement(By.xpath("//li[@title='" + country + "']"));
    }

    public void selectValueFromDropdown(String country) {
        wait.until(ExpectedConditions.visibilityOf(countryDropdown));
        countryDropdown.click();
        countryLink(country).click();
    }

    public String getPageLanguage() {
        return pageLanguage.getAttribute("lang");
    }

}
