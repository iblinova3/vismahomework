package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Driver;

public class PresentationFormPage {

    WebDriver driver;
    WebDriverWait wait = new WebDriverWait(Driver.getInstance(), 10);
    @FindBy(css = "div#wrapperProductLongPageContent h2")
    private WebElement pageTitle;
    @FindBy(xpath = "//input[@name='__field_516545_terms']")
    private WebElement consentCheckbox;
    @FindBy(xpath = "(//span[@data-epiforms-linked-name='__field_516545'])[1]")
    private WebElement consentCheckboxError;
    @FindBy(name = "submit")
    private WebElement submitButton;
    @FindBy(xpath = "//h1")
    private WebElement thankYouHeader;

    public PresentationFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getInputFieldByName(String name) {
        return driver.findElement(By.xpath("//div[@data-eloqua-html-field='" + name + "']/input"));
    }

    public WebElement getErrorByName(String name) {
        return driver.findElement(By.xpath("//div[@data-eloqua-html-field='" + name + "']/span"));
    }

    public void titleIsVisible() {
        wait.until(ExpectedConditions.visibilityOf(pageTitle));
    }

    public void clickSubmitButton() {
        wait.until(ExpectedConditions.visibilityOf(submitButton));
        submitButton.click();
    }

    public String getErrorText(String name) {
        return getErrorByName(name).getText();
    }

    public void inputText(String name, String text) {
        getInputFieldByName(name).sendKeys(text);
    }

    public void checkConsentCheckbox() {
        wait.until(ExpectedConditions.visibilityOf(consentCheckbox));
        consentCheckbox.click();
    }

    public String getConsentCheckbocErrorText() {
        return consentCheckboxError.getText();
    }

    public String getthankYouHeaderText() {
        wait.until(ExpectedConditions.visibilityOf(thankYouHeader));
        return thankYouHeader.getText();
    }
}
