package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Driver {

    public static WebDriver driver;

    //There is no other browser support as this just a homework. In real project we would add
    //paramater from command line, e.g. mvn -Dbrowser=chrome to run tests in chrome and in
    //code below would select create driver based on this solution.
    //In real project we would use selenium grid, set up hub and nodes and use remotewebdriver
    public static WebDriver getInstance() {
        if (driver == null) {
            System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\geckodriver.exe");
            driver = new FirefoxDriver();
        }
        return driver;
    }

    public static void killDriver() {
        driver.quit();
        driver = null;
    }

}
