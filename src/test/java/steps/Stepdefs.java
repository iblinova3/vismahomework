package steps;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import static org.assertj.core.api.Assertions.assertThat;
import org.openqa.selenium.WebDriver;
import pageObjects.HomePage;
import pageObjects.PresentationFormPage;
import utils.Driver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Stepdefs {
    WebDriver driver = Driver.getInstance();
    HomePage homePage = new HomePage(Driver.getInstance());
    PresentationFormPage presentationFormPage = new PresentationFormPage(Driver.getInstance());
    List<String> linkList = new ArrayList<String>();
    private SharedContext sharedContext;

    public Stepdefs(SharedContext sharedContext) {
        this.sharedContext = sharedContext;
    }

    @After
    public void closePage() {
        Driver.killDriver();
    }

    @Given("^user navigates to page (.*)$")
    public void userNavigatesToPageTest(String URL) {
        System.out.println("user navigates to page");
        driver.navigate().to(URL);
    }

    @And("user clicks presentation button")
    public void userClicksPresentationButton() {
        homePage.clickPresentationButton();
    }

    @And("presentation page is opened")
    public void presentationPageIsOpened() {
        presentationFormPage.titleIsVisible();
    }

    @When("user clicks submit button")
    public void userClicksSubmitButton() {
        presentationFormPage.clickSubmitButton();
    }


    @Then("error appear under field")
    public void errorsAppearUnderAllField(DataTable values) {
        List<Map<String, String>> list = values.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); i++) {
            assertThat(presentationFormPage.getErrorText(list.get(i).get("field")).equals(list.get(i).get("error")));
        }
    }

    @When("user fills in data")
    public void userFillsInData(DataTable values) {
        List<Map<String, String>> list = values.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); i++) {
            presentationFormPage.inputText(list.get(i).get("field"), list.get(i).get("data"));
        }
    }

    @Then("error (.*) appears under checkbox")
    public void errorAppearsUnderCheckbox(String error) {
        assertThat(presentationFormPage.getConsentCheckbocErrorText()).isEqualTo(error);
    }

    @When("user clicks check box")
    public void userClicksCheckBox() {
        presentationFormPage.checkConsentCheckbox();
    }


    @Then("form is saved successfully with text (.*)")
    public void formIsSavedSuccessfullyWithTextPaldies(String text) {
        assertThat(presentationFormPage.getthankYouHeaderText()).isEqualTo(text);
    }

    @When("(|goes back and )click on active blog link")
    public void clickOnBlogLink() {
        homePage.getActiveBlogLink();
    }

    @Then("opens blog page in new tab")
    public void newBlogPageOpensInNewTab() throws InterruptedException {
        Thread.sleep(1000);//To avoid sleep can add custom wait for new tab open
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        sharedContext.links = linkList;
        sharedContext.links.add(driver.getCurrentUrl());
        driver.close();
        driver.switchTo().window(tabs2.get(0));
    }

    @Then("opened blog pages should be different")
    public void openedBlogPagesShouldBeDifferent() {
        Boolean allEqual = true;
        for (String link : sharedContext.links) {
            if (!link.equals(sharedContext.links.get(0)))
                allEqual = false;
        }
        assertThat(allEqual).isFalse();
    }

    @When("click on (.*) in footer")
    public void clickOnPageLink(String link) {
        homePage.clickSocialLink(link);
    }

    @Then("page with (.*) is opened")
    public void pageWithUrlIsOpened(String link) throws InterruptedException {
        Thread.sleep(2000);//To avoid sleep can add custom wait for new tab open
        assertThat(driver.getCurrentUrl()).isEqualTo(link);
    }

    @Then("page with (.*) is opened in new tab")
    public void pageWithUrlIsOpenedInNewTab(String link) throws InterruptedException {
        Thread.sleep(4000);//To avoid sleep can add custom wait for new tab open
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        assertThat(driver.getCurrentUrl()).isEqualTo(link);
    }

    @When("I select (.*) from dropdown")
    public void iSelectCountryFromDropdown(String country) {
        homePage.selectValueFromDropdown(country);
    }

    @And("page language is (.*)")
    public void pageLanguageIsLanguage(String language) {
        assertThat(homePage.getPageLanguage()).isEqualTo(language);
    }
}
